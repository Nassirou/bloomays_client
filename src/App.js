import React, { useState, useEffect } from 'react';
import axios from 'axios';

const MissionDisplay = () => {
  const [arriving, setArriving] = useState({});
  const [leaving, setLeaving] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios.get('http://localhost:3000/missions')
      .then(response => {
        setArriving(response.data.arriving);
        setLeaving(response.data.leaving);
        setLoading(false);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des missions:', error);
        setError('Une erreur est survenue lors de la récupération des missions.');
        setLoading(false);
      });
  }, []);

  return (
    <div style={styles.container}>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p style={styles.error}>{error}</p>
      ) : (
        <>
          <div style={styles.section}>
            <h2 style={styles.heading}>Bloomers entrants</h2>
            {Object.entries(arriving).map(([date, missions]) => (
              <div key={date} style={styles.dateSection}>
                <h3 style={styles.dateHeadingArriving}>{date}</h3>
                <ul style={styles.missionList}>
                  {missions.map(mission => (
                    <li key={mission.id} style={styles.missionItem}>
                      {mission.firstname} {mission.lastname}  {mission.beginMission}
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>

          <div style={styles.section}>
            <h2 style={styles.heading}>Bloomers sortants</h2>
            {Object.entries(leaving).map(([date, missions]) => (
              <div key={date} style={styles.dateSection}>
                <h3 style={styles.dateHeadingLeaving}>{date}</h3>
                <ul style={styles.missionList}>
                  {missions.map(mission => (
                    <li key={mission.id} style={styles.missionItem}>
                      {mission.firstname} {mission.lastname} {mission.beginMission}
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    backgroundColor: '#f0f0f0',
  },
  section: {
    margin: '20px',
    padding: '20px',
    backgroundColor: 'white',
    borderRadius: '10px',
    boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)',
  },
  heading: {
    fontSize: '24px',
    marginBottom: '10px',
  },
  dateSection: {
    margin: '10px 0',
  },
  dateHeadingArriving: {
    fontSize: '18px',
    marginBottom: '5px',
    color: 'green', 
  },
  dateHeadingLeaving: {
    fontSize: '18px',
    marginBottom: '5px',
    color: 'red', 
  },
  missionList: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },
  missionItem: {
    marginBottom: '5px',
  },
  error: {
    color: 'red',
  },
};

export default MissionDisplay;
